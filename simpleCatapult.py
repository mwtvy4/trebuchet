__author__ = 'Mike Turner'
import plotly as py
import plotly.graph_objs as go
import numpy
import math
import time
mb = .0051 #mass of beam
m2 = .00194 #mass of load
m1 = m2 * 70 #mass of ballast
L = .167 #length from end
D = 1 #length of ruler
g = 32.2 #gravity

dt = .001 #sec
t = 0 #sec

moI = mb * (L** 2+ (D**2 / 3) - (D*L)) # moment of inertia of beam with offset pivot

alpha = 0
theta = -.6
omega = 0

output = []
while theta < 2:
    alpha = (g/moI) * math.cos(theta) * (m1 * L - m2*(D - L))#degrees
    omega += alpha * dt
    theta += omega * dt
    print(theta)
    t += dt
#    time.sleep(.5)
#    output.append( {'theta': theta, 'tangental V':2} )
    output.append(omega * (D-L))
print(omega * (D-L))
#print(output)
#print(numpy.linspace(0,t,num=(t/dt)).tolist())
trace = go.Scatter(x = numpy.linspace(0,t,num=(t/dt)).tolist() , y = output, mode = 'markers')
layout = dict(title = 'Styled Scatter',
              yaxis = dict(zeroline = False),
              xaxis = dict(zeroline = False)
             )
fig = dict(data = [trace], layout = layout)
py.offline.plot(fig)